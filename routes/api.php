<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Authenticate to require token
Route::post('login', 'Api\Auth\LoginController@authenticate');

Route::group(['middleware' => ['jwt.verify']], function() {
    //User || Generate Token
    Route::get('getUser', 'Api\UserController@getAuthUser');

    //RedPacket
    Route::group(['prefix' => 'redpacket'], function() {
        Route::post('create', 'Api\RedPacketController@create');
        Route::post('receive', 'Api\RedPacketController@receive');
    });
});