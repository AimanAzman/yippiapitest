<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RedPacketModel extends Model
{
    public $table = 'red_packet';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'amount',
        'quantity',
        'random',
        'balance'
    ];

    public function getRedPacket($redpacket_id) {
        return RedPacketModel::where('id', '=', $redpacket_id)->first();
    }

    public function updateRedPacket($id, $balanceAmount) {
        $res = RedPacketModel::find($id);
        $res->update([
            'balance' => $balanceAmount
        ]);
        $res->save();

        return $res;
    }

}
