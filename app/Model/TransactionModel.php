<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TransactionModel extends Model
{
    public $table = 'transaction';
    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'redpacket_id',
        'type',
        'amount',
    ];

    public function getTransaction($redpacket_id) {
        return TransactionModel::where('redpacket_id', '=', $redpacket_id)
                            ->where('type', '=', 2)
                            ->get();
    }

    public function getTransactionByUserID($redpacket_id, $user_id) {
        return TransactionModel::where('redpacket_id', '=', $redpacket_id)
                            ->where('user_id', '=', $user_id)
                            ->where('type', '=', 2)
                            ->get();
    }

    public function makeTransaction($user_id, $redpacket_id, $receiveAmount, $type) {
        $transaction = array(
            'user_id' => $user_id,
            'redpacket_id' => $redpacket_id,
            'amount' => $receiveAmount,
            'type' => $type
        );
        TransactionModel::create($transaction);
    }
}
