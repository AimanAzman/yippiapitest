<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use App\Model\RedPacketModel;
use App\Model\TransactionModel;
use Validator;

class RedPacketController extends Controller
{
    public function create(Request $request) {

        //Validate request data
        $validate = Validator::make($request->all(), [
            'amount' => 'required|numeric|min:0.01',
            'quantity' => 'required|numeric',
            'random' => 'required|boolean'
        ]);

        //Return with error if validate fails
        if($validate->fails()) {
            $message = $validate->errors();
            return $this->response($message, false, 400);
        }

        //Start to create data to save into db
        try {
            $user = $this->getAuthUser()->toArray();
            $redPacket = array(
                'user_id' => $user['id'],
                'amount' => $request->amount,
                'quantity' => $request->quantity,
                'random' => $request->random,
                'balance' => $request->amount
            );
            $redPacket = RedPacketModel::create($redPacket);

            //Create Transaction data
            $transaction = array(
                'user_id' => $user['id'],
                'redpacket_id' => $redPacket->id,
                'type' => '1',
                'amount' => $redPacket->amount
            );
            TransactionModel::create($transaction);

        } catch(\Exception $e) {
            return $this->response($e->getMessage(), false, 400);
        }

        $message = ['redpacket_id' => $redPacket->id];
        return $this->response($message, true, 200);
    }

    public function receive(Request $request) {

        $redPacketRepo = new RedPacketModel();
        $transactionRepo = new TransactionModel();

        $rules = array(
            'redpacket_id' => 'required|numeric',
            'user_id' => 'required|numeric'
        );
        
        //Validate request data
        $validate = Validator::make($request->all(), $rules);

        //Return with error if validate fails
        if($validate->fails()) {
            $message = $validate->errors();
            return $this->response($message, false, 400);
        }

        //check redpacket status
        $check = $this->checkAvailability($request->redpacket_id, $request->user_id);
        if(!$check['res']) {
            return $this->response($check['message'], false, 400);
        }
        

        $redPacketData = $redPacketRepo->getRedPacket($request->redpacket_id);

        $balanceAmount = 0;
        $baseAmount = $redPacketData['amount'];
        $quantity = $redPacketData['quantity'];

        $count = $transactionRepo->getTransaction($request->redpacket_id)->count();
        //check randomize or not
        if($redPacketData['random'] == 1) {
            if($quantity - $count == 1) {
                $receiveAmount = $redPacketData['balance'];
            } else {
                $receiveAmount = rand(0, $baseAmount);
            }
            
        } else {
            //divide equally
            if($quantity - $count == 1) {
                $receiveAmount = $redPacketData['balance'];
            } else {
                $receiveAmount = $baseAmount / $quantity;
            }
        }

        $balanceAmount = $redPacketData['balance'] - $receiveAmount;
        
        //update redpacket table and create transaction
        $redPacketRepo->updateRedPacket($request->redpacket_id, $balanceAmount);
        $transactionRepo->makeTransaction($request->user_id, $request->redpacket_id, $receiveAmount, 2);

        return $this->response(['received' => $receiveAmount], true, 200);
    }

    public function checkAvailability($redpacket_id, $user_id) {
        
        $redPacketRepo = new RedPacketModel();
        $redPacket = $redPacketRepo->getRedPacket($redpacket_id);

        $transactionRepo = new TransactionModel();
        $transaction = $transactionRepo->getTransaction($redpacket_id);
        if($redPacket == null) {
            $message = 'Unable to find RedPacket ID';
            return [
                'res' => false,
                'message' => $message
            ];
        } else {
            $redPacket = $redPacket->toArray();
            if($redPacket['user_id'] == $user_id) {
                $message = 'Cannot redeem RedPacket that you Sent';
                return [
                    'res' => false,
                    'message' => $message
                ];
            }

            if($redPacket['balance'] == 0) {
                $message = 'Failed to Redeem, no available amount left';
                return [
                    'res' => false,
                    'message' => $message
                ];
            }

            if($transaction->count() >= $redPacket['quantity']) {
                $message = 'RedPacket already Exceed quantity limit';
                return [
                    'res' => false,
                    'message' => $message
                ];
                
            }

            $transLimit = $transactionRepo->getTransactionByUserID($redpacket_id, $user_id);
            if($transLimit->isNotEmpty()) {
                $message = 'You have already redeemed the RedPacket';
                return [
                    'res' => false,
                    'message' => $message
                ];
            }
            
        }
        return ['res' => true];
    }
}
