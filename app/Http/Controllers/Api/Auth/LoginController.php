<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Api\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;

class LoginController extends Controller
{
    public function authenticate(Request $request)
    {
        $creds = $request->only(['email', 'password']);

        //Validate request data
        $validate = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        //Return with error if validate fails
        if($validate->fails()) {
            $message = $validate->errors();
            return $this->response($message, false, 400);
        }

        try {
            if(!$token = JWTAuth::attempt($creds)) {
                $message = 'Invalid Email or Password';
                return $this->response($message, false, 400);
            }
        } catch(JWTException $e) {
            $message = 'Could not Create Token';
            return $this->response($message, false, 500);
        }

        $user = auth()->user();

        return response()->json(['success' => $user, 'token' => $token]);
    }
}
