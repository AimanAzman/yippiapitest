<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('users')->delete();
        $password = \Hash::make('asdasd');
        \DB::table('users')->insert([
            [
                'id'        =>  1,
                'name'      =>  'Ahmad',
                'email'     =>  'ahmad@test.com',
                'password'  =>  $password
            ],
            [
                'id'        =>  2,
                'name'      =>  'Lucy',
                'email'     =>  'lucy@test.com',
                'password'  =>  $password
            ],
            [
                'id'        =>  3,
                'name'      =>  'Ramona',
                'email'     =>  'ramona@test.com',
                'password'  =>  $password
            ],
            [
                'id'        =>  4,
                'name'      =>  'Clark',
                'email'     =>  'clark@test.com',
                'password'  =>  $password
            ],
            [
                'id'        =>  5,
                'name'      =>  'Kent',
                'email'     =>  'kent@test.com',
                'password'  =>  $password
            ]
        ]);
    }
}
