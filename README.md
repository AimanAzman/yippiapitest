## Steps

1. Clone the repo
2. Copy .env.example and rename into .env
3. Change .env database details into your db
4. run php artisan key:generate
5. run composer install
6. run npm install && npm run dev
7. run php artisan migrate --seed